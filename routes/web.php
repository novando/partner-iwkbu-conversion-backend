<?php

use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return response()->json(["data" => view('report')->render(), "message" => "DATA_FOUND"], 200);
});