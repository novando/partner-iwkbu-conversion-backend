<?php

use App\Http\Controllers\IwkbuController;
use Illuminate\Support\Facades\Route;

Route::get('/', [IwkbuController::class, 'getAll']);
Route::get('/download-all', [IwkbuController::class, 'downloadAll']);
Route::put('/{id}', [IwkbuController::class, 'update']);
