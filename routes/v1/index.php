<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('iwkbu')->group( function () {
    require 'iwkbu.php';
});
Route::prefix('webhook')->group( function () {
    require 'webhook.php';
});
Route::prefix('official')->group( function () {
    require 'official.php';
});
Route::prefix('reason')->group( function () {
    require 'reason.php';
});
Route::prefix('report')->group( function () {
    require 'report.php';
});
