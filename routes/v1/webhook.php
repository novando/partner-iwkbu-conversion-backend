<?php

use App\Http\Controllers\WebhookController;
use Illuminate\Support\Facades\Route;

Route::post('/iwkbu-before', [WebhookController::class, 'saveScrapedIwkbuBefore']);
Route::post('/iwkbu-current', [WebhookController::class, 'saveScrapedIwkbuCurrent']);
Route::post('/last-3-days', [WebhookController::class, 'saveScrapedIwkbuCurrent']);
