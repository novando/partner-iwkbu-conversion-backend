<?php

use App\Http\Controllers\OfficialController;
use Illuminate\Support\Facades\Route;

Route::get('/', [OfficialController::class, 'get5']);
Route::get('/hq', [OfficialController::class, 'getHq5']);
