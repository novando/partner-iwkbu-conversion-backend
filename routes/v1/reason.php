<?php

use App\Http\Controllers\ReasonController;
use Illuminate\Support\Facades\Route;

Route::get('/', [ReasonController::class, 'getAll']);
