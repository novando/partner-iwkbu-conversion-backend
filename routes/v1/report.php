<?php

use App\Http\Controllers\ReportController;
use App\Http\Controllers\DownloadController;
use Illuminate\Support\Facades\Route;

Route::post("/conversion-summary", [ReportController::class, "conversionSummary"]);
Route::post("/official-summary", [ReportController::class, "officeSummary"]);

// Downloads
Route::post("/conversion-summary/download", [DownloadController::class, "reportConversionSummary"]);
Route::post("/official-summary/download", [DownloadController::class, "reportOfficeSummary"]);