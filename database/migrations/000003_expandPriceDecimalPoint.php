<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table("iwkbu_year_currents", function (Blueprint $table) {
            $table->decimal('price', 12)->change();
        });
        Schema::table("iwkbu_year_befores", function (Blueprint $table) {
            $table->decimal('price', 12)->change();
        });
    }

    public function down(): void
    {
        // pass
    }
};