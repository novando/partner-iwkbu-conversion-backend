<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dasi_users', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->string('username')->unique();
            $table->string('password');
            $table->index('deleted_at');
        });
        Schema::create('official_types', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->string('name')->unique();
            $table->index('deleted_at');
        });
        Schema::create('official_hqs', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->string('code')->unique();
            $table->index('deleted_at');
        });
        Schema::create('official_names', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->string('name')->unique();
            $table->index('deleted_at');
        });
        Schema::create('officials', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->foreignId('official_hq_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('official_type_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('official_name_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->string('code')->unique();
            $table->index('deleted_at');
        });
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->foreignId('official_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->string('nopol')->unique();
            $table->index('deleted_at');
        });
        Schema::create('scores', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->integer('score')->unique();
            $table->string('name')->unique();
            $table->index('deleted_at');
        });
        Schema::create('reasons', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->foreignId('score_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->string('name')->unique();
            $table->text('desc')->nullable();
            $table->index('deleted_at');
        });
        Schema::create('iwkbus', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->foreignId('vehicle_id')->unique()->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->index('deleted_at');
        });
        Schema::create('iwkbu_year_befores', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->foreignId('iwkbu_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->timestampTz('iwkbu_date')->nullable();
            $table->timestampTz('recorded')->nullable();
            $table->timestampTz('swdkllj')->nullable();
            $table->decimal('price');
            $table->index('deleted_at');
        });
        Schema::create('iwkbu_year_currents', function (Blueprint $table) {
            $table->id();
            $table->softDeletesTz();
            $table->timestampsTz();
            $table->foreignId('iwkbu_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('reason_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->timestampTz('iwkbu_date')->nullable();
            $table->timestampTz('recorded')->nullable();
            $table->timestampTz('swdkllj')->nullable();
            $table->decimal('price');
            $table->text('note')->nullable();
            $table->index('deleted_at');
        });
        $dateUtc = new DateTime();
        $cabHqId = DB::table('official_hqs')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'code' => 'CAB']);
        $sraHqId = DB::table('official_hqs')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'code' => 'SRA']);
        $mglHqId = DB::table('official_hqs')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'code' => 'MGL']);
        $pwtHqId = DB::table('official_hqs')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'code' => 'PWT']);
        $pklHqId = DB::table('official_hqs')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'code' => 'PKL']);
        $ptiHqId = DB::table('official_hqs')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'code' => 'PTI']);
        $smgHqId = DB::table('official_hqs')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'code' => 'SMG']);
        $skhHqId = DB::table('official_hqs')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'code' => 'SKH']);

        $indukTypeId = DB::table('official_types')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'Induk']);
        $dtdTypeId = DB::table('official_types')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'DTD']);
        $crmTypeId = DB::table('official_types')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'CRM']);
        $penyalurTypeId = DB::table('official_types')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'Penyalur']);

        $LOKET_CABNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'LOKET CAB']);
        $KENDALNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'KENDAL']);
        $DEMAKNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'DEMAK']);
        $UNGARANNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'UNGARAN']);
        $SALATIGANameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'SALATIGA']);
        $PURWODADINameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'PURWODADI']);
        $LOKET_SURAKARTANameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'LOKET SURAKARTA']);
        $SURAKARTANameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'SURAKARTA']);
        $KLATENNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'KLATEN']);
        $BOYOLALINameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'BOYOLALI']);
        $SRAGENNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'SRAGEN']);
        $PRAMBANANNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'PRAMBANAN']);
        $DELANGGUNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'DELANGGU']);
        $LOKET_MAGELANGNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'LOKET MAGELANG']);
        $MAGELANGNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'MAGELANG']);
        $PURWOREJONameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'PURWOREJO']);
        $KEBUMENNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'KEBUMEN']);
        $TEMANGGUNGNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'TEMANGGUNG']);
        $WONOSOBONameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'WONOSOBO']);
        $MUNGKIDNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'MUNGKID']);
        $BAGELENNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'BAGELEN']);
        $LOKET_PURWOKERTONameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'LOKET PURWOKERTO']);
        $PURWOKERTONameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'PURWOKERTO']);
        $PURBALINGGANameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'PURBALINGGA']);
        $BANJARNEGARANameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'BANJARNEGARA']);
        $CILACAPNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'CILACAP']);
        $MAJENANGNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'MAJENANG']);
        $WANGONNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'WANGON']);
        $LOKET_PEKALONGANNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'LOKET PEKALONGAN']);
        $PEKALONGANNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'PEKALONGAN']);
        $PEMALANGNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'PEMALANG']);
        $TEGALNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'TEGAL']);
        $BREBESNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'BREBES']);
        $BATANGNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'BATANG']);
        $KAJENNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'KAJEN']);
        $SLAWINameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'SLAWI']);
        $BUMIAYUNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'BUMIAYU']);
        $TANJUNGNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'TANJUNG']);
        $LOKET_PATINameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'LOKET PATI']);
        $PATINameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'PATI']);
        $KUDUSNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'KUDUS']);
        $JEPARANameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'JEPARA']);
        $REMBANGNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'REMBANG']);
        $BLORANameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'BLORA']);
        $CEPUNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'CEPU']);
        $LOKET_SEMARANGNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'LOKET SEMARANG']);
        $SEMARANG_INameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'SEMARANG I']);
        $SEMARANG_IINameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'SEMARANG II']);
        $SEMARANG_IIINameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'SEMARANG III']);
        $LOKET_SUKOHARJONameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'LOKET SUKOHARJO']);
        $SUKOHARJONameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'SUKOHARJO']);
        $KARANGANYARNameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'KARANGANYAR']);
        $WONOGIRINameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'WONOGIRI']);
        $BATURETNONameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'BATURETNO']);
        $PURWANTORONameId = DB::table('official_names')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'name' => 'PURWANTORO']);

        DB::table('dasi_users')->insert(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'username' => 'mira.hayuningtyas', 'password' => '@Jateng100']);
        // CAB
        DB::table('officials')->insert([
            // Induk
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_CABNameId, 'code' => '0400001', 'official_hq_id' => $cabHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KENDALNameId, 'code' => '0400009', 'official_hq_id' => $cabHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $DEMAKNameId, 'code' => '0400010', 'official_hq_id' => $cabHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $UNGARANNameId, 'code' => '0400016', 'official_hq_id' => $cabHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SALATIGANameId, 'code' => '0400012', 'official_hq_id' => $cabHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWODADINameId, 'code' => '0400011', 'official_hq_id' => $cabHqId, 'official_type_id' => $indukTypeId],
            // DTD
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_CABNameId, 'code' => '0400038', 'official_hq_id' => $cabHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KENDALNameId, 'code' => '0400045', 'official_hq_id' => $cabHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $DEMAKNameId, 'code' => '0400046', 'official_hq_id' => $cabHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $UNGARANNameId, 'code' => '0400044', 'official_hq_id' => $cabHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SALATIGANameId, 'code' => '0400047', 'official_hq_id' => $cabHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWODADINameId, 'code' => '0400043', 'official_hq_id' => $cabHqId, 'official_type_id' => $dtdTypeId],
            // CRM
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_CABNameId, 'code' => '0400041', 'official_hq_id' => $cabHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KENDALNameId, 'code' => '0400050', 'official_hq_id' => $cabHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $DEMAKNameId, 'code' => '0400051', 'official_hq_id' => $cabHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $UNGARANNameId, 'code' => '0400049', 'official_hq_id' => $cabHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SALATIGANameId, 'code' => '0400052', 'official_hq_id' => $cabHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWODADINameId, 'code' => '0400048', 'official_hq_id' => $cabHqId, 'official_type_id' => $crmTypeId],
            // Penyalur
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_CABNameId, 'code' => '0400003', 'official_hq_id' => $cabHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KENDALNameId, 'code' => '0400004', 'official_hq_id' => $cabHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $DEMAKNameId, 'code' => '0400005', 'official_hq_id' => $cabHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $UNGARANNameId, 'code' => '0400007', 'official_hq_id' => $cabHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SALATIGANameId, 'code' => '0400008', 'official_hq_id' => $cabHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWODADINameId, 'code' => '0400006', 'official_hq_id' => $cabHqId, 'official_type_id' => $penyalurTypeId],
        ]);

        // SRA
        DB::table('officials')->insert([
            // Induk
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SURAKARTANameId, 'code' => '0400101', 'official_hq_id' => $sraHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SURAKARTANameId, 'code' => '0400110', 'official_hq_id' => $sraHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KLATENNameId, 'code' => '0400112', 'official_hq_id' => $sraHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BOYOLALINameId, 'code' => '0400113', 'official_hq_id' => $sraHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SRAGENNameId, 'code' => '0400114', 'official_hq_id' => $sraHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PRAMBANANNameId, 'code' => '0400118', 'official_hq_id' => $sraHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $DELANGGUNameId, 'code' => '0400122', 'official_hq_id' => $sraHqId, 'official_type_id' => $indukTypeId],
            // DTD
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SURAKARTANameId, 'code' => '0400129', 'official_hq_id' => $sraHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SURAKARTANameId, 'code' => '0400132', 'official_hq_id' => $sraHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KLATENNameId, 'code' => '0400135', 'official_hq_id' => $sraHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BOYOLALINameId, 'code' => '0400134', 'official_hq_id' => $sraHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SRAGENNameId, 'code' => '0400133', 'official_hq_id' => $sraHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PRAMBANANNameId, 'code' => '0400137', 'official_hq_id' => $sraHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $DELANGGUNameId, 'code' => '0400136', 'official_hq_id' => $sraHqId, 'official_type_id' => $dtdTypeId],
            // CRM
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SURAKARTANameId, 'code' => '0400131', 'official_hq_id' => $sraHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SURAKARTANameId, 'code' => '0400138', 'official_hq_id' => $sraHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KLATENNameId, 'code' => '0400141', 'official_hq_id' => $sraHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BOYOLALINameId, 'code' => '0400140', 'official_hq_id' => $sraHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SRAGENNameId, 'code' => '0400139', 'official_hq_id' => $sraHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PRAMBANANNameId, 'code' => '0400143', 'official_hq_id' => $sraHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $DELANGGUNameId, 'code' => '0400142', 'official_hq_id' => $sraHqId, 'official_type_id' => $crmTypeId],
            // Penyalur
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SURAKARTANameId, 'code' => '0400103', 'official_hq_id' => $sraHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SURAKARTANameId, 'code' => '0400124', 'official_hq_id' => $sraHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KLATENNameId, 'code' => '0400105', 'official_hq_id' => $sraHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BOYOLALINameId, 'code' => '0400106', 'official_hq_id' => $sraHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SRAGENNameId, 'code' => '0400107', 'official_hq_id' => $sraHqId, 'official_type_id' => $penyalurTypeId],
        ]);

        // MGL
        DB::table('officials')->insert([
            // Induk
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_MAGELANGNameId, 'code' => '0400201', 'official_hq_id' => $mglHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MAGELANGNameId, 'code' => '0400209', 'official_hq_id' => $mglHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWOREJONameId, 'code' => '0400210', 'official_hq_id' => $mglHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KEBUMENNameId, 'code' => '0400211', 'official_hq_id' => $mglHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TEMANGGUNGNameId, 'code' => '0400212', 'official_hq_id' => $mglHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WONOSOBONameId, 'code' => '0400213', 'official_hq_id' => $mglHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MUNGKIDNameId, 'code' => '0400215', 'official_hq_id' => $mglHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BAGELENNameId, 'code' => '0400216', 'official_hq_id' => $mglHqId, 'official_type_id' => $indukTypeId],
            // DTD
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_MAGELANGNameId, 'code' => '0400226', 'official_hq_id' => $mglHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MAGELANGNameId, 'code' => '0400229', 'official_hq_id' => $mglHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWOREJONameId, 'code' => '0400234', 'official_hq_id' => $mglHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KEBUMENNameId, 'code' => '0400230', 'official_hq_id' => $mglHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TEMANGGUNGNameId, 'code' => '0400232', 'official_hq_id' => $mglHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WONOSOBONameId, 'code' => '0400233', 'official_hq_id' => $mglHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MUNGKIDNameId, 'code' => '0400231', 'official_hq_id' => $mglHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BAGELENNameId, 'code' => '0400235', 'official_hq_id' => $mglHqId, 'official_type_id' => $dtdTypeId],
            // CRM
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_MAGELANGNameId, 'code' => '0400228', 'official_hq_id' => $mglHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MAGELANGNameId, 'code' => '0400236', 'official_hq_id' => $mglHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWOREJONameId, 'code' => '0400241', 'official_hq_id' => $mglHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KEBUMENNameId, 'code' => '0400237', 'official_hq_id' => $mglHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TEMANGGUNGNameId, 'code' => '0400239', 'official_hq_id' => $mglHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WONOSOBONameId, 'code' => '0400240', 'official_hq_id' => $mglHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MUNGKIDNameId, 'code' => '0400238', 'official_hq_id' => $mglHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BAGELENNameId, 'code' => '0400242', 'official_hq_id' => $mglHqId, 'official_type_id' => $crmTypeId],
            // Penyalur
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_MAGELANGNameId, 'code' => '0400208', 'official_hq_id' => $mglHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MAGELANGNameId, 'code' => '0400202', 'official_hq_id' => $mglHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWOREJONameId, 'code' => '0400204', 'official_hq_id' => $mglHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KEBUMENNameId, 'code' => '0400205', 'official_hq_id' => $mglHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TEMANGGUNGNameId, 'code' => '0400206', 'official_hq_id' => $mglHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WONOSOBONameId, 'code' => '0400207', 'official_hq_id' => $mglHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MUNGKIDNameId, 'code' => '0400203', 'official_hq_id' => $mglHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BAGELENNameId, 'code' => '0400200', 'official_hq_id' => $mglHqId, 'official_type_id' => $penyalurTypeId],
        ]);

        // PWT
        DB::table('officials')->insert([
            // Induk
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PURWOKERTONameId, 'code' => '0400301', 'official_hq_id' => $pwtHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWOKERTONameId, 'code' => '0400303', 'official_hq_id' => $pwtHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURBALINGGANameId, 'code' => '0400304', 'official_hq_id' => $pwtHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BANJARNEGARANameId, 'code' => '0400305', 'official_hq_id' => $pwtHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $CILACAPNameId, 'code' => '0400307', 'official_hq_id' => $pwtHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MAJENANGNameId, 'code' => '0400306', 'official_hq_id' => $pwtHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WANGONNameId, 'code' => '0400318', 'official_hq_id' => $pwtHqId, 'official_type_id' => $indukTypeId],
            // DTD
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PURWOKERTONameId, 'code' => '0400326', 'official_hq_id' => $pwtHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWOKERTONameId, 'code' => '0400331', 'official_hq_id' => $pwtHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURBALINGGANameId, 'code' => '0400333', 'official_hq_id' => $pwtHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BANJARNEGARANameId, 'code' => '0400334', 'official_hq_id' => $pwtHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $CILACAPNameId, 'code' => '0400332', 'official_hq_id' => $pwtHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MAJENANGNameId, 'code' => '0400335', 'official_hq_id' => $pwtHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WANGONNameId, 'code' => '0400336', 'official_hq_id' => $pwtHqId, 'official_type_id' => $dtdTypeId],
            // CRM
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PURWOKERTONameId, 'code' => '0400329', 'official_hq_id' => $pwtHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWOKERTONameId, 'code' => '0400337', 'official_hq_id' => $pwtHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURBALINGGANameId, 'code' => '0400339', 'official_hq_id' => $pwtHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BANJARNEGARANameId, 'code' => '0400340', 'official_hq_id' => $pwtHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $CILACAPNameId, 'code' => '0400338', 'official_hq_id' => $pwtHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $MAJENANGNameId, 'code' => '0400341', 'official_hq_id' => $pwtHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WANGONNameId, 'code' => '0400342', 'official_hq_id' => $pwtHqId, 'official_type_id' => $crmTypeId],
            // Penyalur
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PURWOKERTONameId, 'code' => '0400317', 'official_hq_id' => $pwtHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWOKERTONameId, 'code' => '0400308', 'official_hq_id' => $pwtHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURBALINGGANameId, 'code' => '0400314', 'official_hq_id' => $pwtHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BANJARNEGARANameId, 'code' => '300315', 'official_hq_id' => $pwtHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $CILACAPNameId, 'code' => '0400312', 'official_hq_id' => $pwtHqId, 'official_type_id' => $penyalurTypeId],
        ]);

        // PKL
        DB::table('officials')->insert([
            // Induk
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PEKALONGANNameId, 'code' => '0400401', 'official_hq_id' => $pklHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PEKALONGANNameId, 'code' => '0400408', 'official_hq_id' => $pklHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PEMALANGNameId, 'code' => '0400409', 'official_hq_id' => $pklHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TEGALNameId, 'code' => '0400410', 'official_hq_id' => $pklHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BREBESNameId, 'code' => '0400411', 'official_hq_id' => $pklHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BATANGNameId, 'code' => '0400412', 'official_hq_id' => $pklHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KAJENNameId, 'code' => '0400417', 'official_hq_id' => $pklHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SLAWINameId, 'code' => '0400418', 'official_hq_id' => $pklHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BUMIAYUNameId, 'code' => '0400419', 'official_hq_id' => $pklHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TANJUNGNameId, 'code' => '0400420', 'official_hq_id' => $pklHqId, 'official_type_id' => $indukTypeId],
            // DTD
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PEKALONGANNameId, 'code' => '0400428', 'official_hq_id' => $pklHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PEKALONGANNameId, 'code' => '0400432', 'official_hq_id' => $pklHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PEMALANGNameId, 'code' => '0400433', 'official_hq_id' => $pklHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TEGALNameId, 'code' => '0400438', 'official_hq_id' => $pklHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BREBESNameId, 'code' => '0400437', 'official_hq_id' => $pklHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BATANGNameId, 'code' => '0400436', 'official_hq_id' => $pklHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KAJENNameId, 'code' => '0400435', 'official_hq_id' => $pklHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SLAWINameId, 'code' => '0400434', 'official_hq_id' => $pklHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BUMIAYUNameId, 'code' => '0400439', 'official_hq_id' => $pklHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TANJUNGNameId, 'code' => '0400440', 'official_hq_id' => $pklHqId, 'official_type_id' => $dtdTypeId],
            // CRM
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PEKALONGANNameId, 'code' => '0400430', 'official_hq_id' => $pklHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PEKALONGANNameId, 'code' => '0400441', 'official_hq_id' => $pklHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PEMALANGNameId, 'code' => '0400442', 'official_hq_id' => $pklHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TEGALNameId, 'code' => '0400447', 'official_hq_id' => $pklHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BREBESNameId, 'code' => '0400446', 'official_hq_id' => $pklHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BATANGNameId, 'code' => '0400445', 'official_hq_id' => $pklHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KAJENNameId, 'code' => '0400444', 'official_hq_id' => $pklHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SLAWINameId, 'code' => '0400443', 'official_hq_id' => $pklHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BUMIAYUNameId, 'code' => '0400448', 'official_hq_id' => $pklHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TANJUNGNameId, 'code' => '0400449', 'official_hq_id' => $pklHqId, 'official_type_id' => $crmTypeId],
            // Penyalur
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PEKALONGANNameId, 'code' => '0400421', 'official_hq_id' => $pklHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PEKALONGANNameId, 'code' => '0400402', 'official_hq_id' => $pklHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PEMALANGNameId, 'code' => '0400404', 'official_hq_id' => $pklHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $TEGALNameId, 'code' => '0400405', 'official_hq_id' => $pklHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BREBESNameId, 'code' => '0400407', 'official_hq_id' => $pklHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BATANGNameId, 'code' => '0400403', 'official_hq_id' => $pklHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KAJENNameId, 'code' => '0400415', 'official_hq_id' => $pklHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SLAWINameId, 'code' => '0400406', 'official_hq_id' => $pklHqId, 'official_type_id' => $penyalurTypeId],
        ]);

        // PTI
        DB::table('officials')->insert([
            // Induk
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PATINameId, 'code' => '0400501', 'official_hq_id' => $ptiHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PATINameId, 'code' => '0400507', 'official_hq_id' => $ptiHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KUDUSNameId, 'code' => '0400508', 'official_hq_id' => $ptiHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $JEPARANameId, 'code' => '0400509', 'official_hq_id' => $ptiHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $REMBANGNameId, 'code' => '0400510', 'official_hq_id' => $ptiHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BLORANameId, 'code' => '0400511', 'official_hq_id' => $ptiHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $CEPUNameId, 'code' => '0400515', 'official_hq_id' => $ptiHqId, 'official_type_id' => $indukTypeId],
            // DTD
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PATINameId, 'code' => '0400526', 'official_hq_id' => $ptiHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PATINameId, 'code' => '0400533', 'official_hq_id' => $ptiHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KUDUSNameId, 'code' => '0400531', 'official_hq_id' => $ptiHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $JEPARANameId, 'code' => '0400532', 'official_hq_id' => $ptiHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $REMBANGNameId, 'code' => '0400534', 'official_hq_id' => $ptiHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BLORANameId, 'code' => '0400535', 'official_hq_id' => $ptiHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $CEPUNameId, 'code' => '0400536', 'official_hq_id' => $ptiHqId, 'official_type_id' => $dtdTypeId],
            // CRM
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PATINameId, 'code' => '0400530', 'official_hq_id' => $ptiHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PATINameId, 'code' => '0400540', 'official_hq_id' => $ptiHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KUDUSNameId, 'code' => '0400538', 'official_hq_id' => $ptiHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $JEPARANameId, 'code' => '0400539', 'official_hq_id' => $ptiHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $REMBANGNameId, 'code' => '0400541', 'official_hq_id' => $ptiHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BLORANameId, 'code' => '0400542', 'official_hq_id' => $ptiHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $CEPUNameId, 'code' => '0400543', 'official_hq_id' => $ptiHqId, 'official_type_id' => $crmTypeId],
            // Penyalur
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_PATINameId, 'code' => '0400517', 'official_hq_id' => $ptiHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PATINameId, 'code' => '0400502', 'official_hq_id' => $ptiHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KUDUSNameId, 'code' => '0400503', 'official_hq_id' => $ptiHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $JEPARANameId, 'code' => '0400504', 'official_hq_id' => $ptiHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $REMBANGNameId, 'code' => '0400505', 'official_hq_id' => $ptiHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BLORANameId, 'code' => '0400506', 'official_hq_id' => $ptiHqId, 'official_type_id' => $penyalurTypeId],
        ]);

        // SMG
        DB::table('officials')->insert([
            // Induk
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SEMARANGNameId, 'code' => '0400601', 'official_hq_id' => $smgHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SEMARANG_INameId, 'code' => '0400602', 'official_hq_id' => $smgHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SEMARANG_IINameId, 'code' => '0400603', 'official_hq_id' => $smgHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SEMARANG_IIINameId, 'code' => '0400604', 'official_hq_id' => $smgHqId, 'official_type_id' => $indukTypeId],
            // DTD
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SEMARANGNameId, 'code' => '0400614', 'official_hq_id' => $smgHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SEMARANG_INameId, 'code' => '0400617', 'official_hq_id' => $smgHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SEMARANG_IINameId, 'code' => '0400618', 'official_hq_id' => $smgHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SEMARANG_IIINameId, 'code' => '0400619', 'official_hq_id' => $smgHqId, 'official_type_id' => $dtdTypeId],
            // CRM
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SEMARANGNameId, 'code' => '0400615', 'official_hq_id' => $smgHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SEMARANG_INameId, 'code' => '0400620', 'official_hq_id' => $smgHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SEMARANG_IINameId, 'code' => '0400621', 'official_hq_id' => $smgHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SEMARANG_IIINameId, 'code' => '0400622', 'official_hq_id' => $smgHqId, 'official_type_id' => $crmTypeId],
            // Penyalur
        ]);

        // SKH
        DB::table('officials')->insert([
            // Induk
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SUKOHARJONameId, 'code' => '0400701', 'official_hq_id' => $skhHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SUKOHARJONameId, 'code' => '0400702', 'official_hq_id' => $skhHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KARANGANYARNameId, 'code' => '0400703', 'official_hq_id' => $skhHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WONOGIRINameId, 'code' => '0400704', 'official_hq_id' => $skhHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BATURETNONameId, 'code' => '0400706', 'official_hq_id' => $skhHqId, 'official_type_id' => $indukTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWANTORONameId, 'code' => '0400705', 'official_hq_id' => $skhHqId, 'official_type_id' => $indukTypeId],
            // DTD
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SUKOHARJONameId, 'code' => '0400715', 'official_hq_id' => $skhHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SUKOHARJONameId, 'code' => '0400718', 'official_hq_id' => $skhHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KARANGANYARNameId, 'code' => '0400719', 'official_hq_id' => $skhHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WONOGIRINameId, 'code' => '0400720', 'official_hq_id' => $skhHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BATURETNONameId, 'code' => '0400722', 'official_hq_id' => $skhHqId, 'official_type_id' => $dtdTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWANTORONameId, 'code' => '0400721', 'official_hq_id' => $skhHqId, 'official_type_id' => $dtdTypeId],
            // CRM
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SUKOHARJONameId, 'code' => '0400717', 'official_hq_id' => $skhHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SUKOHARJONameId, 'code' => '0400723', 'official_hq_id' => $skhHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KARANGANYARNameId, 'code' => '0400724', 'official_hq_id' => $skhHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WONOGIRINameId, 'code' => '0400725', 'official_hq_id' => $skhHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $BATURETNONameId, 'code' => '0400727', 'official_hq_id' => $skhHqId, 'official_type_id' => $crmTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $PURWANTORONameId, 'code' => '0400726', 'official_hq_id' => $skhHqId, 'official_type_id' => $crmTypeId],
            // Penyalur
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $LOKET_SUKOHARJONameId, 'code' => '0400707', 'official_hq_id' => $skhHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $SUKOHARJONameId, 'code' => '0400709', 'official_hq_id' => $skhHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $KARANGANYARNameId, 'code' => '0400710', 'official_hq_id' => $skhHqId, 'official_type_id' => $penyalurTypeId],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'official_name_id' => $WONOGIRINameId, 'code' => '0400708', 'official_hq_id' => $skhHqId, 'official_type_id' => $penyalurTypeId],
        ]);

        $goodScoreId = DB::table('scores')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score' => 100, 'name' => 'GOOD']);
        $neutralScoreId = DB::table('scores')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score' => 0, 'name' => 'NEUTRAL']);
        $badScoreId = DB::table('scores')->insertGetId(['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score' => -100, 'name' => 'BAD']);
        DB::table('reasons')->insert([
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $neutralScoreId, 'name' => 'TANPA CATATAN', 'desc' => null],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $goodScoreId, 'name' => 'PELUNASAN', 'desc' => 'Penerimaan armada yang membayar IWKBU di bulan yang sama tahun ini & tahun lalu'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $goodScoreId, 'name' => 'ARMADA BARU', 'desc' => 'Penerimaan IWKBU armada baru (potensi baru)'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $goodScoreId, 'name' => 'MUTASI DATANG', 'desc' => 'Penerimaan IWKBU dari armada yang melakukan transaksi mutasi datang dari luar wilayah'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $goodScoreId, 'name' => 'PEMBAYARAN BARU (NOPOL LAMA)', 'desc' => 'Penerimaan armada (bukan armada baru) yang membayar IWKBU di bulan ini tahun ini tapi tidak membayar IWKBU di bulan yang sama tahun lalu'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $neutralScoreId, 'name' => 'MUTASI UBAH TARIF', 'desc' => 'Selisih nilai IWKBU akibat perbedaan tarif dari proses mutasi ubah tarif antara tarif baru & tarif lama (bisa + / -)'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'GANTI NOPOL BARU', 'desc' => 'Armada yang saat ini tidak membayar IWKBU karena telah ganti nopol, namun di bulan yang sama tahun lalu membayar IWKBU'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'RUSAK SEMENTARA', 'desc' => 'Armada yang saat ini tidak membayar IWKBU karena pemeliharaan rusak sementara, namun di bulan yang sama tahun lalu membayar IWKBU'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'CADANGAN', 'desc' => 'Armada yang saat ini tidak membayar IWKBU karena pemeliharaan cadangan, namun di bulan yang sama tahun lalu membayar IWKBU'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'PEMUTIHAN / DISPENSASI', 'desc' => 'Armada yang saat ini tidak membayar IWKBU karena adanya dispensasi IWKBU, namun di bulan yang sama tahun lalu membayar IWKBU'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'BAYAR DI LUAR WILAYAH / LAINNYA', 'desc' => 'Armada yang saat ini tidak membayar IWKBU karena telah melakukan pembayaran di luar wilayah, namun di bulan yang sama tahun lalu membayar IWKBU di dalam wilayah'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'MUTASI KELUAR', 'desc' => 'Armada yang saat ini tidak membayar IWKBU karena mutasi ke luar wilayah, namun di bulan yang sama tahun lalu membayar IWKBU'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'RUSAK SELAMANYA (BESI TUA)', 'desc' => 'Armada yang saat ini tidak membayar IWKBU karena pemeliharaan rusak selamanya (besi tua) tidak beroperasi lagi, namun di bulan yang sama tahun lalu membayar IWKBU'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'UBAH SIFAT', 'desc' => 'Armada yang saat ini tidak membayar IWKBU karena telah ubah sifat ke plat hitam, namun di bulan yang sama tahun lalu membayar IWKBU'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'UBAH BENTUK', 'desc' => 'Armada yang saat ini tidak membayar IWKBU karena telah ubah bentuk & tidak lagi mengangkut penumpang, namun di bulan yang sama tahun lalu membayar IWKBU'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'TAHUN LALU MEMBAYAR TUNGGAKAN', 'desc' => 'Armada yang saat ini tidak membayar IWKBU namun di bulan yang sama tahun lalu hanya membayar tunggakan IWKBU'],
            ['created_at' => $dateUtc->format('Y-m-d H:i:s'), 'updated_at' => $dateUtc->format('Y-m-d H:i:s'), 'score_id' => $badScoreId, 'name' => 'IWKBU MASIH BERLAKU', 'desc' => 'Armada yang saat ini tidak membayar IWKBU karena di periode berbeda telah melakukan pembayaran IWKBU & masih berlaku'],
            
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('iwkbu_year_currents');
        Schema::dropIfExists('iwkbu_year_befores');
        Schema::dropIfExists('iwkbus');
        Schema::dropIfExists('reasons');
        Schema::dropIfExists('scores');
        Schema::dropIfExists('vehicles');
        Schema::dropIfExists('officials');
        Schema::dropIfExists('official_names');
        Schema::dropIfExists('official_hqs');
        Schema::dropIfExists('official_types');
        Schema::dropIfExists('dasi_users');
    }
};
