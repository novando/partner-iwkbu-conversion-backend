<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::raw("UPDATE iwkbus JOIN vehicles ON iwkbus.vehicle_id = vehicles.id SET iwkbus.official_id = vehicles.official_id");
    }

    public function down(): void
    {
        DB::raw("UPDATE iwkbus SET iwkbus.official_id = null");
    }
};