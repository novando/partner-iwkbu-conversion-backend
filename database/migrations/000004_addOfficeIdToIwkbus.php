<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table("iwkbus", function (Blueprint $table) {
            $table->foreignId("official_id")->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->dropForeign(["vehicle_id"]);
            $table->dropUnique(["vehicle_id"]);
            $table->foreign("vehicle_id")
                ->references("id")
                ->on("vehicles")
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
        DB::table("iwkbus")
            ->join("vehicles", "iwkbus.vehicle_id", "=", "vehicles.id")
            ->groupBy("iwkbus.vehicle_id", "vehicles.name")
            ->update(["iwkbus.official_id" => DB::raw("vehicles.official_id")]);
    }

    public function down(): void
    {
        Schema::table("iwkbus", function (Blueprint $table) {
            $table->dropForeign("iwkbus_official_id_foreign");
            $table->dropIndex("iwkbus_official_id_foreign");
            $table->dropColumn("official_id");
            $table->unique("vehicle_id");
        });
    }
};