<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table("iwkbu_year_currents", function (Blueprint $table) {
            $table->timestampTz("inputed_on")->nullable();
        });
        DB::table("iwkbu_year_currents")
            ->update(["inputed_on" => DB::raw("recorded")]);
    }

    public function down(): void
    {
        Schema::table("iwkbu_year_currents", function (Blueprint $table) {
            $table->dropColumn("inputed_on");
        });
    }
};