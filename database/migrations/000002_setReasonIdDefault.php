<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table("iwkbu_year_currents", function (Blueprint $table) {
            $table->renameColumn('reason_id', 'reason_bak');
            $table->dropForeign('iwkbu_year_currents_reason_id_foreign');
            $table->dropIndex('iwkbu_year_currents_reason_id_foreign');
        });
        Schema::table("iwkbu_year_currents", function (Blueprint $table) {
            $table->foreignId("reason_id")
                ->nullable()
                ->constrained()
                ->cascadeOnUpdate()
                ->nullOnDelete()
                ->default(1);
        });
        DB::raw("UPDATE iwkbu_year_currents SET reason_id = reason_bak");
        DB::table("iwkbu_year_currents")
            ->where("reason_bak", null)
            ->update(["reason_id" => 1]);
        Schema::table("iwkbu_year_currents", function (Blueprint $table) {
            $table->dropColumn('reason_bak');
        });
    }

    public function down(): void
    {
        Schema::table("iwkbu_year_currents", function (Blueprint $table) {
            $table->renameColumn('reason_id', 'reason_bak');
            $table->dropForeign('iwkbu_year_currents_reason_id_foreign');
            $table->dropIndex('iwkbu_year_currents_reason_id_foreign');
        });
        Schema::table("iwkbu_year_currents", function (Blueprint $table) {
            $table->foreignId("reason_id")
                ->nullable()
                ->constrained()
                ->cascadeOnUpdate()
                ->nullOnDelete();
        });
        DB::raw("UPDATE iwkbu_year_currents SET reason_id = reason_bak");
        Schema::table("iwkbu_year_currents", function (Blueprint $table) {
            $table->dropColumn('reason_bak');
        });
    }
};