<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table("iwkbu_year_befores", function (Blueprint $table) {
            $table->foreignId("reason_id")->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete();
            $table->text("note")->nullable();
        });
    }

    public function down(): void
    {
        Schema::table("iwkbu_year_befores", function (Blueprint $table) {
            $table->dropForeign("iwkbu_year_befores_reason_id_foreign");
            $table->dropIndex("iwkbu_year_befores_reason_id_foreign");
            $table->dropColumn("note");
            $table->dropColumn("reason_id");
        });
    }
};
