@php
  // use App\Helpers\NumberFormatterHelper;
@endphp

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
  <title>Laporan IWKBU per Loket</title>
  <style>
    /*! tailwindcss v3.3.3 | MIT License | https://tailwindcss.com*/*,:after,:before{box-sizing:border-box;border:0 solid #e5e7eb}:after,:before{--tw-content:""}html{line-height:1.5;-webkit-text-size-adjust:100%;-moz-tab-size:4;-o-tab-size:4;tab-size:4;font-family:ui-sans-serif,system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;font-feature-settings:normal;font-variation-settings:normal}body{margin:0;line-height:inherit}hr{height:0;color:inherit;border-top-width:1px}abbr:where([title]){-webkit-text-decoration:underline dotted;text-decoration:underline dotted}h1,h2,h3,h4,h5,h6{font-size:inherit;font-weight:inherit}a{color:inherit;text-decoration:inherit}b,strong{font-weight:bolder}code,kbd,pre,samp{font-family:ui-monospace,SFMono-Regular,Menlo,Monaco,Consolas,Liberation Mono,Courier New,monospace;font-size:1em}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:initial}sub{bottom:-.25em}sup{top:-.5em}table{text-indent:0;border-color:inherit;border-collapse:collapse}button,input,optgroup,select,textarea{font-family:inherit;font-feature-settings:inherit;font-variation-settings:inherit;font-size:100%;font-weight:inherit;line-height:inherit;color:inherit;margin:0;padding:0}button,select{text-transform:none}[type=button],[type=reset],[type=submit],button{-webkit-appearance:button;background-color:initial;background-image:none}:-moz-focusring{outline:auto}:-moz-ui-invalid{box-shadow:none}progress{vertical-align:initial}::-webkit-inner-spin-button,::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}summary{display:list-item}blockquote,dd,dl,figure,h1,h2,h3,h4,h5,h6,hr,p,pre{margin:0}fieldset{margin:0}fieldset,legend{padding:0}menu,ol,ul{list-style:none;margin:0;padding:0}dialog{padding:0}textarea{resize:vertical}input::-moz-placeholder,textarea::-moz-placeholder{opacity:1;color:#9ca3af}input::placeholder,textarea::placeholder{opacity:1;color:#9ca3af}[role=button],button{cursor:pointer}:disabled{cursor:default}audio,canvas,embed,iframe,img,object,svg,video{display:block;vertical-align:middle}img,video{max-width:100%;height:auto}[hidden]{display:none}html{--tw-bg-opacity:1;background-color:rgb(255 255 255/var(--tw-bg-opacity))}body{font-family:Roboto,sans-serif;font-size:.875rem;line-height:1.25rem;--tw-text-opacity:1;color:rgb(70 71 73/var(--tw-text-opacity))}*,::backdrop,:after,:before{--tw-border-spacing-x:0;--tw-border-spacing-y:0;--tw-translate-x:0;--tw-translate-y:0;--tw-rotate:0;--tw-skew-x:0;--tw-skew-y:0;--tw-scale-x:1;--tw-scale-y:1;--tw-pan-x: ;--tw-pan-y: ;--tw-pinch-zoom: ;--tw-scroll-snap-strictness:proximity;--tw-gradient-from-position: ;--tw-gradient-via-position: ;--tw-gradient-to-position: ;--tw-ordinal: ;--tw-slashed-zero: ;--tw-numeric-figure: ;--tw-numeric-spacing: ;--tw-numeric-fraction: ;--tw-ring-inset: ;--tw-ring-offset-width:0px;--tw-ring-offset-color:#fff;--tw-ring-color:#3b82f680;--tw-ring-offset-shadow:0 0 #0000;--tw-ring-shadow:0 0 #0000;--tw-shadow:0 0 #0000;--tw-shadow-colored:0 0 #0000;--tw-blur: ;--tw-brightness: ;--tw-contrast: ;--tw-grayscale: ;--tw-hue-rotate: ;--tw-invert: ;--tw-saturate: ;--tw-sepia: ;--tw-drop-shadow: ;--tw-backdrop-blur: ;--tw-backdrop-brightness: ;--tw-backdrop-contrast: ;--tw-backdrop-grayscale: ;--tw-backdrop-hue-rotate: ;--tw-backdrop-invert: ;--tw-backdrop-opacity: ;--tw-backdrop-saturate: ;--tw-backdrop-sepia: }.mb-16{margin-bottom:4rem}.mb-32{margin-bottom:8rem}.mb-\[16px\]{margin-bottom:16px}.mb-\[32px\]{margin-bottom:32px}.flex{display:flex}.table{display:table}.w-\[124px\]{width:124px}.w-\[21cm\]{width:21cm}.w-full{width:100%}.min-w-\[100px\]{min-width:100px}.items-center{align-items:center}.justify-between{justify-content:space-between}.gap-\[120px\]{gap:120px}.rounded-lg{border-radius:.5rem}.border-b{border-bottom-width:1px}.border-black{--tw-border-opacity:1;border-color:rgb(0 0 0/var(--tw-border-opacity))}.bg-\[\#590995\]{--tw-bg-opacity:1;background-color:rgb(89 9 149/var(--tw-bg-opacity))}.bg-\[\#760CC5\]{--tw-bg-opacity:1;background-color:rgb(118 12 197/var(--tw-bg-opacity))}.bg-\[\#cdcdcd\]{--tw-bg-opacity:1;background-color:rgb(205 205 205/var(--tw-bg-opacity))}.bg-primary{--tw-bg-opacity:1;background-color:rgb(255 151 0/var(--tw-bg-opacity))}.bg-white{--tw-bg-opacity:1;background-color:rgb(255 255 255/var(--tw-bg-opacity))}.p-\[16px\]{padding:16px}.px-16{padding-left:4rem;padding-right:4rem}.px-\[16px\]{padding-left:16px;padding-right:16px}.py-4{padding-top:1rem;padding-bottom:1rem}.py-8{padding-top:2rem;padding-bottom:2rem}.py-\[2px\]{padding-top:2px;padding-bottom:2px}.py-\[4px\]{padding-top:4px;padding-bottom:4px}.text-center{text-align:center}.text-end{text-align:end}.text-2xl{font-size:1.5rem;line-height:2rem}.text-\[8px\]{font-size:8px}.text-xl{font-size:1.25rem;line-height:1.75rem}.font-bold{font-weight:700}.text-white{--tw-text-opacity:1;color:rgb(255 255 255/var(--tw-text-opacity))}
  </style>
</head>
<body>
  <main>
    <section class="bg-white w-[21cm] p-[16px]">
      <section class="flex justify-between items-center border-b border-black mb-[32px]">
        <div class="w-[124px]">
          <img src="https://jasaraharja.co.id/images/global/JR%20new.png" />
        </div>
        <div class="text-center">
          <h1 class="font-bold text-2xl">Check In - Check Out</h1>
          <h2 class="font-bold text-xl">Konversi Penerimaan IWKBU</h2>
        </div>
        <div class="w-[124px]"/>
      </section>
      <section class="flex gap-[120px] mb-[16px]">
        <div>
          <label class="flex">
            <p class="min-w-[100px]">Dokumen</p>
            <p>: Konversi per Loket</p>
          </label>
          <label class="flex">
            <p class="min-w-[100px]">Kantor</p>
            <p>: {{ $hq ? $hq : "SEMUA" }}</p>
          </label>
          <label class="flex">
            <p class="min-w-[100px]">Loket</p>
            <p>: {{ $office ? $office : "SEMUA" }}</p>
          </label>
        </div>
        <div>
          <label class="flex">
            <p class="min-w-[100px]">Awal Periode</p>
            <p>: {{ $since }}</p>
          </label>
          <label class="flex">
            <p class="min-w-[100px]">Akhir Periode</p>
            <p>: {{ $till }}</p>
          </label>
          <label class="flex">
            <p class="min-w-[100px]">Waktu Cetak</p>
            <p>: {{ $now }}</p>
          </label>
        </div>
      </section>
      <table class="rounded-lg w-full">
        <thead>
          <tr class="text-[8px] bg-[#760CC5]">
            <th class="text-[8px] text-white py-[4px]">Loket</th>
            <th class="text-[8px] text-white py-[4px]">Samsat</th>
            <th class="text-[8px] text-white py-[4px]">Jumlah Nopol Tahun Lalu</th>
            <th class="text-[8px] text-white py-[4px]">Nominal IWKBU Tahun Lalu</th>
            <th class="text-[8px] text-white py-[4px]">Jumlah Nopol Tahun Ini</th>
            <th class="text-[8px] text-white py-[4px]">Nominal IWKBU Tahun Ini</th>
            <th class="text-[8px] text-white py-[4px]">Outstanding Nopol</th>
            <th class="text-[8px] text-white py-[4px]">Outstanding Penerimaan</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($officeData as $datum)
            <tr @class(['text-[8px]', 'bg-[#cdcdcd]' => $loop->even, 'bg-white' => $loop->odd])>
              <td class="text-[8px] text-center px-[16px] py-[2px]">{{ $datum["hq"] }}</td>
              <td class="text-[8px] text-center px-[16px] py-[2px]">{{ $datum["office"] }}</td>
              <td class="text-[8px] text-center px-[16px] py-[2px]">{{ $datum["lastQty"] }}</td>
              <td class="text-[8px] text-end px-[16px] py-[2px]">{{ $datum["lastIncome"] }}</td>
              <td class="text-[8px] text-center px-[16px] py-[2px]">{{ $datum["currQty"] }}</td>
              <td class="text-[8px] text-end px-[16px] py-[2px]">{{ $datum["currIncome"] }}</td>
              <td class="text-[8px] text-center px-[16px] py-[2px]">{{ $datum["sumQty"] }}</td>
              <td class="text-[8px] text-end px-[16px] py-[2px]">{{ $datum["sumIncome"] }}</td>
            </tr>
          @endforeach
          <tr class="text-[8px] bg-[#590995] text-white">
            <td class="text-[8px] text-center px-[16px] py-[2px]" colspan="2">Grand total</td>
            <td class="text-[8px] text-center px-[16px] py-[2px]">{{ array_reduce($officeData, function($acc, $item) {return $acc + $item["lastQty"];}, 0) }}</td>
            <td class="text-[8px] text-end px-[16px] py-[2px]">{{ array_reduce($officeData, function($acc, $item) {return $acc + $item["lastIncome"];}, 0) }}</td>
            <td class="text-[8px] text-center px-[16px] py-[2px]">{{ array_reduce($officeData, function($acc, $item) {return $acc + $item["currQty"];}, 0) }}</td>
            <td class="text-[8px] text-end px-[16px] py-[2px]">{{ array_reduce($officeData, function($acc, $item) {return $acc + $item["currIncome"];}, 0) }}</td>
            <td class="text-[8px] text-center px-[16px] py-[2px]">{{ array_reduce($officeData, function($acc, $item) {return $acc + $item["sumQty"];}, 0) }}</td>
            <td class="text-[8px] text-end px-[16px] py-[2px]">{{ array_reduce($officeData, function($acc, $item) {return $acc + $item["sumIncome"];}, 0) }}</td>
          </tr>
        </tbody>
      </table>
    </section>
  </main>
</body>
</html>