<?php

namespace App\Helpers;

use DateTime;
use DateTimeZone;

class DateHelper
{
    public static function dateToIso(string $datetime, $tz = "Asia/Jakarta")
    {
        $timezone = new DateTimeZone($tz);
        $date = new DateTime($datetime, $timezone);
        $newDate = $date->setTimeZone($timezone);
        return $newDate->format("c");
    }

    public static function dateToMonthNumberString(string $datetime, $tz = "Asia/Jakarta")
    {
        $timezone = new DateTimeZone($tz);
        $date = new DateTime($datetime, $timezone);
        $newDate = $date->setTimeZone($timezone);
        return (int)$newDate->format("m");
    }

    public static function modifyDateToIso(string $datetime, string $modifier, $tz = "Asia/Jakarta")
    {
        $timezone = new DateTimeZone($tz);
        $date = new DateTime($datetime, $timezone);
        $newDate = $date->modify($modifier)->setTimeZone($timezone);
        return $newDate->format("c");
    }

    public static function modifyDateToMonthNumberString(string $datetime, string $modifier, $tz = "Asia/Jakarta")
    {
        $timezone = new DateTimeZone($tz);
        $date = new DateTime($datetime, $timezone);
        $newDate = $date->modify($modifier)->setTimeZone($timezone);
        return (int)$newDate->format("m");
    }

    public static function endOfMonthIsoString(string $iso, $tz = "Asia/Jakarta")
    {
        $datetime = date("Y-m-t\TH:i:sP", strtotime($iso));
        $date = new DateTime($datetime);
        $newDate = $date->setTimeZone(new DateTimeZone($tz));
        return $newDate->format("Y-m-t\T23:59:59P");
    }

    public static function endOfDayIsoString(string $iso, $tz = "Asia/Jakarta")
    {
        $datetime = date("Y-m-d\TH:i:sP", strtotime($iso));
        $date = new DateTime($datetime);
        $newDate = $date->setTimeZone(new DateTimeZone($tz));
        return $newDate->format("Y-m-d\T23:59:59P");
    }

    public static function startOfMonthIsoString(string $iso, $tz = "Asia/Jakarta")
    {
        $date = new DateTime($iso);
        $newDate = $date->setTimeZone(new DateTimeZone($tz));
        return $newDate->format("Y-m-01\T00:00:00P");
    }

    public static function startOfDayIsoString(string $iso, $tz = "Asia/Jakarta")
    {
        $date = new DateTime($iso);
        $newDate = $date->setTimeZone(new DateTimeZone($tz));
        return $newDate->format("Y-m-d\T00:00:00P");
    }

    public static function isoToMariaDateString(string $iso, $tz = "Asia/Jakarta")
    {
        $date = new DateTime($iso);
        $newDate = $date->setTimeZone(new DateTimeZone($tz));
        return $newDate->format("Y-m-d H:i:s");
    }

    public static function isoToFullMonthDateString(string $iso, $tz = "Asia/Jakarta")
    {
        $date = new DateTime($iso);
        $newDate = $date->setTimeZone(new DateTimeZone($tz));
        return $newDate->format("j F Y");
    }

    public static function isoToFullDateTimeString(string $iso, $tz = "Asia/Jakarta")
    {
        $date = new DateTime($iso);
        $newDate = $date->setTimeZone(new DateTimeZone($tz));
        return $newDate->format("j F Y H:i:s");
    }

    public static function monthDateToLastYearRangeMariaArrayString(string $datetime): array
    {
        $originDate = DateHelper::modifyDateToIso($datetime, "-1 Year");
        $sinceIso = DateHelper::startOfMonthIsoString($originDate);
        $tillIso = DateHelper::endOfMonthIsoString($originDate);
        $since = DateHelper::isoToMariaDateString($sinceIso);
        $till = DateHelper::isoToMariaDateString($tillIso);
        return [$since, $till];
    }

    public static function monthDateToRangeMariaArrayString(string $datetime): array
    {
        $sinceIso = DateHelper::startOfMonthIsoString($datetime);
        $tillIso = DateHelper::endOfMonthIsoString($datetime);
        $since = DateHelper::isoToMariaDateString($sinceIso);
        $till = DateHelper::isoToMariaDateString($tillIso);
        return [$since, $till];
    }

    public static function rangeDateToRangeMariaArrayString(string $sinceDate, string $tillDate): array
    {
        $sinceIso = DateHelper::startOfDayIsoString($sinceDate);
        $tillIso = DateHelper::endOfDayIsoString($tillDate);
        $since = DateHelper::isoToMariaDateString($sinceIso);
        $till = DateHelper::isoToMariaDateString($tillIso);
        return [$since, $till];
    }

    public static function rangeDateToLastYearRangeMariaArrayString(string $sinceDate, string $tillDate): array
    {
        $sinceOrigin = DateHelper::modifyDateToIso($sinceDate, "-1 Year");
        $tillOrigin = DateHelper::modifyDateToIso($tillDate, "-1 Year");
        $sinceIso = DateHelper::startOfDayIsoString($sinceOrigin);
        $tillIso = DateHelper::endOfDayIsoString($tillOrigin);
        $since = DateHelper::isoToMariaDateString($sinceIso);
        $till = DateHelper::isoToMariaDateString($tillIso);
        return [$since, $till];
    }

    public static function mariaDateToDate(string $mariaDate) {
        $date = DateTime::createFromFormat("Y-m-d H:i:s", $mariaDate);
        return $date;
    }
}