<?php

namespace App\Helpers;

class HttpRequestHelper
{
    /**
     * cURL with POST Method withoud authentication.
     * 
     * Send HTTP Request using cURL with POST method
     * only use if the endpoint can be accessed publicly.
     * 
     * @param string $url The endpoint that the request will be sent to.
     * @param array $payload The data that will be sent. 
     * 
     * @return mixed Response from the server, usually JSON formatted text.
     */
    public static function curlPost(string $url, array $payload) {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($payload),
            CURLOPT_HTTPHEADER => ["Content-Type: application/json"],
        ]);
        $res = curl_exec($curl);
        curl_close($curl);
        return $res;
    }
}