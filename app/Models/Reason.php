<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Reason extends Model
{
    use SoftDeletes;
    
    protected $tables = ['reasons'];
    protected $fillable = ['score_id', 'name', 'desc'];

    public function score(): BelongsTo
    {
        return $this->belongsTo(Score::class);
    }
    public function iwkbuYearCurrent(): HasMany
    {
        return $this->hasMany(IwkbuYearCurrent::class);
    }
}
