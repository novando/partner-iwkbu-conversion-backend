<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Vehicle extends Model
{
    use SoftDeletes;

    protected $tables = ['vehicles'];
    protected $fillable = ['nopol', 'official_id'];

    public function official(): BelongsTo
    {
        return $this->belongsTo(Official::class);
    }
    public function iwkbu(): HasMany
    {
        return $this->hasMany(Iwkbu::class);
    }
}
