<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Iwkbu extends Model
{
    use SoftDeletes;

    protected $tables = ["iwkbus"];
    protected $fillable = ["vehicle_id", "official_id"];

    public function vehicle(): BelongsTo
    {
        return $this->belongsTo(Vehicle::class);
    }
    public function iwkbuYearBefores(): HasMany
    {
        return $this->hasMany(IwkbuYearBefore::class);
    }
    public function iwkbuYearCurrents(): HasMany
    {
        return $this->hasMany(IwkbuYearCurrent::class);
    }
    public function official(): BelongsTo
    {
        return $this->belongsTo(Official::class);
    }
}
