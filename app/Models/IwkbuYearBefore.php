<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class IwkbuYearBefore extends Model
{
    use SoftDeletes;

    protected $tables = ['iwkbu_year_befores'];
    protected $fillable = [
        'iwkbu_id',
        'iwkbu_date',
        'reason_id',
        'recorded',
        'swdkllj',
        'price',
        'note',
    ];

    public function iwkbu(): BelongsTo
    {
      return $this->belongsTo(Iwkbu::class);
    }
    public function reason(): BelongsTo
    {
        return $this->belongsTo(Reason::class);
    }
}
