<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class DasiUser extends Model
{
    use SoftDeletes;
    
    protected $tables = ['dasi_users'];
    protected $fillable = ['username', 'password'];
}
