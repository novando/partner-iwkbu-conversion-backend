<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Official extends Model
{
    use SoftDeletes;
    
    protected $tables = ['officials'];
    protected $fillable = [
      'official_hq_id',
      'official_type_id',
      'official_name_id',
      'code',
    ];

    public function officialHq(): BelongsTo
    {
        return $this->belongsTo(OfficialHq::class);
    }
    public function officialType(): BelongsTo
    {
        return $this->belongsTo(OfficialType::class);
    }
    public function officialName(): BelongsTo
    {
        return $this->belongsTo(OfficialName::class);
    }
    public function vehicles(): HasMany
    {
        return $this->hasMany(Vehicle::class);
    }
    public function iwkbu(): HasMany
    {
        return $this->hasMany(Vehicle::class);
    }
}
