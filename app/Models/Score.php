<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Score extends Model
{
    use SoftDeletes;
    
    protected $tables = ['reasons'];
    protected $fillable = ['score', 'name'];

    public function reasons(): HasMany
    {
        return $this->hasMany(Reason::class);
    }
}
