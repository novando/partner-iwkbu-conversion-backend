<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class OfficialHq extends Model
{
    use SoftDeletes;
    
    protected $tables = ['official_hqs'];
    protected $fillable = ['code'];

    public function officials(): HasMany
    {
        return $this->hasMany(Official::class);
    }
}
