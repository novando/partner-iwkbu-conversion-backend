<?php

namespace App\Http\Controllers;

use App\Models\Iwkbu;
use App\Models\IwkbuYearCurrent;
use App\Models\Vehicle;
use App\Models\OfficialName;
use App\Models\OfficialHq;
use Illuminate\Http\Request;

class OfficialController extends Controller
{
    public function get5(Request $req)
    {
        $search = $req->query("search");
        $hq = $req->query("hq");
        $officialQuery = OfficialName::SelectRaw("official_names.id as id, name")
            ->join("officials", "official_names.id", "=", "officials.official_name_id")
            ->join("official_hqs", "official_hqs.id", "=", "officials.official_hq_id")
            ->where("name", "LIKE", "%$search%")
            ->groupBy("name", "official_names.id");
        if (strlen($hq) > 0) $officialQuery = $officialQuery->where("official_hqs.code", "=", $hq);
        $officialData = $officialQuery->limit(5)->get();
        $officialDto = [];
        foreach ($officialData as $datum) {
            $dto["id"] = $datum["id"];
            $dto["name"] = $datum["name"];
            array_push($officialDto, $dto);
        }
        return response()->json(["data" => $officialDto, "message" => "DATA_FOUND"], 200);
    }
    public function getHq5(Request $req)
    {
        $search = $req->query("search");
        $officialQuery = OfficialHq::where("code", "LIKE", "%$search%");
        $officialCount = $officialQuery->count();
        $officialData = $officialQuery->limit(5)->get();
        $officialDto = [];
        foreach ($officialData as $datum) {
            $dto["id"] = $datum["id"];
            $dto["code"] = $datum["code"];
            array_push($officialDto, $dto);
        }
        return response()->json(["count" => $officialCount, "data" => $officialDto, "message" => "DATA_FOUND"], 200);
    }
}
