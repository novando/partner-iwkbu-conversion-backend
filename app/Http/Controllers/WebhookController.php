<?php

namespace App\Http\Controllers;

use App\Models\Iwkbu;
use App\Models\IwkbuYearBefore;
use App\Models\IwkbuYearCurrent;
use App\Models\Vehicle;
use App\Models\Official;
use App\Helpers\DateHelper;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DateTime;

class WebhookController extends Controller
{
    public function saveScrapedIwkbuBefore(Request $req)
    {
        try {
            $officialData = Official::where("code", $req["kantor"])->first();
            $vehicleData = Vehicle::firstOrCreate(["nopol" => $req["nopol"]], ["official_id" => $officialData["id"]]);
            $iwkbuData = Iwkbu::firstOrCreate(["vehicle_id" => $vehicleData->id, "official_id" => $officialData["id"]]);
            $iwkbuYearData = [
                "iwkbu_id" => $iwkbuData->id,
                "iwkbu_date" => DateHelper::isoToMariaDateString($req["iwkbu"]),
                "recorded" => DateHelper::isoToMariaDateString($req["recorded"]),
                "price" => (double)$req["price"],
            ];
            $iwkbuTransactionData = [
                "swdkllj" => DateHelper::isoToMariaDateString($req["swdkllj"]),
            ];
            IwkbuYearBefore::updateOrCreate($iwkbuYearData, $iwkbuTransactionData);
            return response()->json(["data" => null, "message" => "VEHICLE_SAVED"], 200);
        } catch (Exeption $e) {
            return response()->json(["data" => null, "message" => $e->getMessage()], 500);
        }
    }
    public function saveScrapedIwkbuCurrent(Request $req)
    {
        $officialData = Official::where("code", $req["kantor"])->first();
        $vehicleData = Vehicle::firstOrCreate(["nopol" => $req["nopol"]], ["official_id" => $officialData["id"]]);
        $iwkbuData = Iwkbu::firstOrCreate(["vehicle_id" => $vehicleData->id, "official_id" => $officialData["id"]]);
        $iwkbuDate = Carbon::parse($req["iwkbu"])->format('Y-m-d H:i:s');
        $recordedDate = Carbon::parse($req["recorded"])->format('Y-m-d H:i:s');
        $iwkbuQuery = IwkbuYearCurrent::where("iwkbu_id", $iwkbuData->id)
            ->whereRaw("CAST(iwkbu_date AS Date) = '$iwkbuDate'")
            ->whereRaw("CAST(recorded AS Date) = '$recordedDate'")
            ->orderBy("updated_at", "DESC");
        $iwkbuCurrentData = $iwkbuQuery->first();
        $iwkbuTransactionData = [
            "iwkbu_id" => $iwkbuData->id,
            "iwkbu_date" => $iwkbuDate,
            "recorded" => $recordedDate,
            "price" => (double)$req["price"],
        ];
        $iwkbuUserInputted = [
            "swdkllj" => Carbon::parse($req["swdkllj"])->format('Y-m-d H:i:s'),
        ];
        if ($iwkbuCurrentData) {
            $iwkbuUserInputted["note"] = $iwkbuCurrentData["note"];
            $iwkbuUserInputted["reason_id"] = $iwkbuCurrentData["reason_id"];
        }
        try {
            IwkbuYearBefore::updateOrCreate($iwkbuTransactionData, $iwkbuUserInputted);
            return response()->json(["data" => null, "message" => "VEHICLE_SAVED"], 200);
        } catch (Exeption $e) {
            return response()->json(["data" => null, "message" => $e->getMessage()], 500);
        }
    }
}
