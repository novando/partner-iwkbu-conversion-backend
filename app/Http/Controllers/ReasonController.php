<?php

namespace App\Http\Controllers;

use App\Models\Reason;
use Illuminate\Http\Request;

class ReasonController extends Controller
{
    public function getAll(Request $req)
    {
        $reasonData = Reason::with('score')->get();
        $reasonDto = [];
        foreach ($reasonData as $datum) {
            $dto["id"] = $datum["id"];
            $dto["score"] = $datum->score["score"];
            $dto["name"] = $datum["name"];
            $dto["desc"] = $datum["desc"];
            array_push($reasonDto, $dto);
        }
        return response()->json(["data" => $reasonDto, "message" => "DATA_FOUND"], 200);
    }
}
