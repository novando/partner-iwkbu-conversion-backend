<?php

namespace App\Http\Controllers;

use App\Models\Iwkbu;
use App\Models\IwkbuYearCurrent;
use App\Models\IwkbuYearBefore;
use App\Models\Vehicle;
use App\Models\Official;
use App\Models\Reason;
use Illuminate\Http\Request;
use App\Helpers\DateHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class IwkbuController extends Controller
{
    /** TODO Optimize the function,
     * 1) Query need update
     * 2) Remove nested For Loop
     * */
    protected static function downloadSettings ($text) {
        $headers = [
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=export.csv",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        ];
        $callback = function() use($text) {
            $file = fopen('php://output', 'w');
            fwrite($file, $text);
            fclose($file);
        };
        return [$callback, $headers];
    }

    public function getAll(Request $req)
    {
        $searchOfficial = $req->query("official");
        [$lastSince, $lastTill] = DateHelper::monthDateToLastYearRangeMariaArrayString($req->query("period"));
        [$currSince, $currTill] = DateHelper::monthDateToRangeMariaArrayString($req->query("period"));
        $lastDateSince = Carbon::parse($lastSince)->format('Y-m-d');
        $lastDateTill = Carbon::parse($lastTill)->format('Y-m-d');
        $currDateSince = Carbon::parse($currSince)->format('Y-m-d');
        $currDateTill = Carbon::parse($currTill)->format('Y-m-d');
        $lastYear = Carbon::parse($lastTill)->year;
        $currYear = Carbon::parse($currTill)->year;
        $iwkbuData = DB::select("
            SELECT
                MAX(iw.id) AS id,
                v.nopol AS nopol,
                MAX(CASE WHEN YEAR(recorded) = $lastYear THEN recorded ELSE '1970-01-01 00:00:00' END) AS recorded_before,
                MAX(CASE WHEN YEAR(recorded) = $lastYear THEN iwkbu_date ELSE '1970-01-01 00:00:00' END) AS iwkbu_date_before,
                MAX(CASE WHEN YEAR(recorded) = $lastYear THEN swdkllj ELSE '1970-01-01 00:00:00' END) AS swdkllj_before,
                MAX(CASE WHEN YEAR(recorded) = $lastYear THEN price ELSE 0 END) AS price_before,
                MAX(CASE WHEN YEAR(recorded) = $currYear THEN recorded ELSE '1970-01-01 00:00:00' END) AS recorded_current,
                MAX(CASE WHEN YEAR(recorded) = $currYear THEN iwkbu_date ELSE '1970-01-01 00:00:00' END) AS iwkbu_date_current,
                MAX(CASE WHEN YEAR(recorded) = $currYear THEN swdkllj ELSE '1970-01-01 00:00:00' END) AS swdkllj_current,
                MAX(CASE WHEN YEAR(recorded) = $currYear THEN price ELSE 0 END) AS price_current,
                MAX(CASE WHEN YEAR(recorded) = 2023 THEN reason_id ELSE 1 END) AS reason_id,
                MAX(note) AS note
            FROM (
                SELECT * FROM iwkbu_year_befores WHERE CAST(recorded AS DATE) BETWEEN '$lastDateSince' AND '$lastDateTill'
                UNION
                SELECT * FROM iwkbu_year_befores WHERE CAST(recorded AS DATE) BETWEEN '$currDateSince' AND '$currDateTill'
            ) iw
            JOIN iwkbus i ON i.id = iw.iwkbu_id
            JOIN vehicles v ON v.id = i.vehicle_id
            JOIN officials o ON o.id = i.official_id
            JOIN official_names ofcn ON ofcn.id = o.official_name_id
            WHERE ofcn.name = '$searchOfficial'
            GROUP BY iw.iwkbu_id");
        $payload = [];
        foreach ($iwkbuData as $datum) {
            $dto["id"] = $datum->id;
            $dto["nopol"] = $datum->nopol;
            $dto["lastRecorded"] = DateHelper::dateToIso($datum->recorded_before);
            $dto["lastSwdkllj"] = DateHelper::dateToIso($datum->swdkllj_before);
            $dto["lastIwkbu"] = DateHelper::dateToIso($datum->iwkbu_date_before);
            $dto["lastPrice"] = (double)$datum->price_before;
            $dto["currRecorded"] = DateHelper::dateToIso($datum->recorded_current);
            $dto["currSwdkllj"] = DateHelper::dateToIso($datum->swdkllj_current);
            $dto["currIwkbu"] = DateHelper::dateToIso($datum->iwkbu_date_current);
            $dto["currPrice"] = (double)$datum->price_current;
            $dto["reasonId"] = $datum->reason_id;
            $dto["note"] = $datum->note;
            $dto["diffPrice"] = $dto["currPrice"] - $dto["lastPrice"];
            array_push($payload, $dto);
        }
        $sortedPayload = collect($payload)->sortBy([["lastRecorded", "asc"],["currRecorded", "asc"]])->values()->all();
        $resMsg = count($iwkbuData) > 0 ? "IWKBU_FOUND" : "IWKBU_NOT_FOUND";
        return response()->json(["count" => count($iwkbuData), "data" => $sortedPayload, "message" => $resMsg], 200);
    }


    public function downloadAll(Request $req)
    {
        $searchOfficial = $req->query("official");
        $nopol = $req->query("nopol");
        [$lastSince, $lastTill] = DateHelper::monthDateToLastYearRangeMariaArrayString($req->query("period"));
        [$currSince, $currTill] = DateHelper::monthDateToRangeMariaArrayString($req->query("period"));
        $currDateSince = DateHelper::mariaDateToDate($currSince);
        $currDateTill = DateHelper::mariaDateToDate($currTill);
        $iwkbuQuery = Iwkbu::with("iwkbuYearBefores", "official.officialName", "official.officialHq", "iwkbuYearCurrents", "vehicle")
            ->whereHas("iwkbuYearBefores", function($query) use($lastSince, $lastTill) {
                $query->whereBetween("recorded", [$lastSince, $lastTill]);
            })
            ->orWhere(function($query) use($searchOfficial, $currSince, $currTill) {
                $query->whereHas("iwkbuYearCurrents", function($query) use($currSince, $currTill) {
                    $query->whereBetween("inputed_on", [$currSince, $currTill]);
                });
            });
        $reasonData = Reason::orderBy("id")->get();
        foreach ($reasonData as $datum) {
            $reasons["reason" . $datum["id"]] = $datum["name"];
        }
        $iwkbuData = $iwkbuQuery->distinct()->get();
        $payload = [];
        foreach ($iwkbuData as $datum) {
            $dto["id"] = $datum["id"];
            $dto["nopol"] = $datum->vehicle["nopol"];
            $dto["office"] = $datum->official->officialName["name"];
            $dto["hq"] = $datum->official->officialHq["code"];
            $dto["lastRecorded"] = "-";
            $dto["lastSwdkllj"] = "-";
            $dto["lastIwkbu"] = "-";
            $dto["lastPrice"] = 0;
            $dto["currRecorded"] = "-";
            $dto["currSwdkllj"] = "-";
            $dto["currIwkbu"] = "-";
            $dto["currPrice"] = 0;
            $dto["reason"] = $reasons["reason" . 1];
            $dto["note"] = "";
            foreach ($datum->iwkbuYearCurrents as $after) {
                $inputedDate = DateHelper::mariaDateToDate($after["inputed_on"]);
                if ($inputedDate <= $currDateSince || $inputedDate > $currDateTill) continue;
                $dto["currRecorded"] = (new Carbon($after["recorded"]))->format("Y-m-d");
                $dto["currSwdkllj"] = (new Carbon($after["swdkllj"]))->format("Y-m-d");
                $dto["currIwkbu"] = (new Carbon($after["iwkbu_date"]))->format("Y-m-d");
                $dto["currPrice"] += (int)$after["price"];
                $dto["reason"] = $reasons["reason" . $after["reason_id"]];
                $dto["note"] = $after["note"];
            }
            foreach ($datum->iwkbuYearBefores as $before) {
                if (DateHelper::mariaDateToDate($before["recorded"]) <  DateHelper::mariaDateToDate($lastSince) || DateHelper::mariaDateToDate($before["recorded"]) >=  DateHelper::mariaDateToDate($lastTill)) continue;
                $dto["lastRecorded"] = (new Carbon($before["recorded"]))->format("Y-m-d");
                $dto["lastSwdkllj"] = (new Carbon($before["swdkllj"]))->format("Y-m-d");
                $dto["lastIwkbu"] = (new Carbon($before["iwkbu_date"]))->format("Y-m-d");
                $dto["lastPrice"] += (float)$before["price"];
            }
            $dto["diffPrice"] = $dto["currPrice"] - $dto["lastPrice"];
            array_push($payload, $dto);
        }
        $sortedPayload = collect($payload)->sortBy([["lastRecorded", "asc"],["currRecorded", "asc"]])->values()->all();
        $resMsg = count($iwkbuData) > 0 ? "IWKBU_FOUND" : "IWKBU_NOT_FOUND";
        // create csv from $sortedPayload
        $csv = "";
        $csv .= "Nopol; Perwakilan; Samsat; Tanggal Transaksi Sebelum; SWDKLLJ Sebelum; IWKBU Sebelum; Penerimaan Sebelum; Tanggal Transaksi Sekarang; SWDKLLJ Sekarang; IWKBU Sekarang; Penerimaan Sekarang; Outsanding Penerimaan; Konversi IWKBU; Keterangan;\n";
        foreach ($sortedPayload as $datum) {
            $csv .= $datum["nopol"] . ";";
            $csv .= $datum["hq"] . ";";
            $csv .= $datum["office"] . ";";
            $csv .= $datum["lastRecorded"] . ";";
            $csv .= $datum["lastSwdkllj"] . ";";
            $csv .= $datum["lastIwkbu"] . ";";
            $csv .= $datum["lastPrice"] . ";";
            $csv .= $datum["currRecorded"] . ";";
            $csv .= $datum["currSwdkllj"] . ";";
            $csv .= $datum["currIwkbu"] . ";";
            $csv .= $datum["currPrice"] . ";";
            $csv .= $datum["reason"] . ";";
            $csv .= $datum["note"] . ";\n";
        }
        $dlSetting = $this->downloadSettings($csv);
        $filename = "KonversiIWKBU_" . $req->query("period") . ".csv";
        return response()->streamDownload($dlSetting[0], $filename, $dlSetting[1]);
    }

    /** TODO Optimize the function,
     * 1) Inputed on should be calculated form backend
     * */
    public function update(Request $req, $id)
    {
        $payload = [
            "reason_id" => (int)$req["reasonId"],
            "note" => $req["note"],
        ];
        $query = IwkbuYearBefore::where("id", $id);
        $iwkbuData = $query->first();
        if (Carbon::parse($iwkbuData["recorded"])->year !== Carbon::now()->year) return response()->json(["data" => null, "message" => "NO_CURRENT_RECORDED"], 400);
        $query->update($payload);
        return response()->json(["data" => null, "message" => "IWKBU_UPDATED"], 200);
    }
}
