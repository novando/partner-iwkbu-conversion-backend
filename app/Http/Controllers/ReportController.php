<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Reason;
use App\Models\Official;
use App\Models\Iwkbu;
use App\Models\IwkbuYearBefore;
use App\Models\IwkbuYearCurrent;
use App\Helpers\DateHelper;
use App\Services\ReportService;

class ReportController extends Controller
{
    public function conversionSummary(Request $req)
    {
        [$lastSince, $lastTill] = DateHelper::rangeDateToLastYearRangeMariaArrayString($req["since"], $req["till"]);
        [$currSince, $currTill] = DateHelper::rangeDateToRangeMariaArrayString($req["since"], $req["till"]);
        $hqCode = $req['hq'];
        $officialName = $req['office'];
        [$iwkbuIds, $detailDto] = ReportService::conversionSummary($lastSince, $lastTill, $hqCode, $officialName);
        if (count($iwkbuIds->toArray()) < 1) return response()->json(["data" => ["summary" => ["lastIncome" => 0, "lastNopol" => 0, "currIncome" => 0, "currNopol" => 0], "details" => []], "message" => "DATA_NOT_FOUND"], 200);
        $iwkbuBeforeData = IwkbuYearBefore::selectRaw("COUNT(DISTINCT iwkbu_id) as nopol, SUM(price) as price")
            ->whereBetween("recorded", [$lastSince, $lastTill])
            ->whereIn("iwkbu_id", $iwkbuIds)
            ->first();
        $iwkbuCurrentData = IwkbuYearBefore::selectRaw("COUNT(DISTINCT iwkbu_id) as nopol, SUM(price) as price")
            ->whereBetween("recorded",[$currSince, $currTill])
            ->whereIn("iwkbu_id", $iwkbuIds)
            ->first();
        $reportDto = [
            "summary" => [
                "lastIncome" => (double)$iwkbuBeforeData["price"],
                "lastNopol" => $iwkbuBeforeData["nopol"],
                "currIncome" => (double)$iwkbuCurrentData["price"],
                "currNopol" => $iwkbuCurrentData["nopol"],
            ],
            "details" => $detailDto,
        ];
        return response()->json(["data" => $reportDto, "message" => "CONVERSION_REPORT_GENERATED"], 200);
    }
    public function officeSummary(Request $req)
    {
        $reportDto = ReportService::officeSummary($req["since"], $req["till"], $req["hq"], $req["office"]);
        return response()->json(["data" => $reportDto, "message" => "OFFICE_REPORT_GENERATED"], 200);
    }
}
