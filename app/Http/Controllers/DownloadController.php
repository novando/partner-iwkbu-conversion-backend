<?php

namespace App\Http\Controllers;

use App\Services\ReportService;
use App\Helpers\DateHelper;
use App\Helpers\HttpRequestHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DownloadController extends Controller
{
    public function reportOfficeSummary(Request $req)
    {
        try {
            $reportDto = ReportService::officeSummary($req["since"], $req["till"], $req["hq"], $req["office"]);
            $html = view('office-report', [
                "since" => DateHelper::isoToFullMonthDateString($req["since"]),
                "till" => DateHelper::isoToFullMonthDateString($req["till"]),
                "now" => DateHelper::isoToFullDateTimeString("now"),
                "hq" => $req["hq"],
                "office" => $req["office"],
                "officeData" => $reportDto,
            ])->render();
            $url = env("SCRAPER_URL") . "/generate/pdf";
            $res = Http::post($url, ["html" => $html]);
            if ($res->successful()) {
                $pdfData = $res->body();
    
                // You can also return the saved file as a response, if needed
                return response($pdfData)
                    ->header('Content-Type', 'application/pdf')
                    ->header('Content-Disposition', 'attachment; filename="your-pdf-file.pdf"');
            } else {
                // Handle the case when the request to Express was not successful
                return response()->json(["data" => $res, "message" => "DOWNLOAD_PDF_FAILED"], $res->status());
            }
        } catch (\Exception $e) {
            return response()->json(["data" => $e, "message" => "DOWNLOAD_PDF_FAILED"], 500);
        }
    }

    public function reportConversionSummary(Request $req)
    {
        try {
            [$lastSince, $lastTill] = DateHelper::rangeDateToLastYearRangeMariaArrayString($req["since"], $req["till"]);
            [$iwkbuIds, $reportDto] = ReportService::conversionSummary($lastSince, $lastTill, $req["hq"], $req["office"]);
            $topData = array_filter($reportDto, function($item) {
                return in_array($item["name"], ["PELUNASAN", "TANPA CATATAN"]);
            });
            $botData = array_filter($reportDto, function($item) {
                return !in_array($item["name"], ["PELUNASAN", "TANPA CATATAN"]);
            });
            $html = view('conversion-report', [
                "since" => DateHelper::isoToFullMonthDateString($req["since"]),
                "till" => DateHelper::isoToFullMonthDateString($req["till"]),
                "now" => DateHelper::isoToFullDateTimeString("now"),
                "nowDate" => DateHelper::isoToFullMonthDateString("now"),
                "hq" => $req["hq"],
                "office" => $req["office"],
                "topData" => $topData,
                "botData" => $botData,
            ])->render();
            $url = env("SCRAPER_URL") . "/generate/pdf";
            $res = Http::post($url, ["html" => $html]);
            if ($res->successful()) {
                $pdfData = $res->body();
    
                // You can also return the saved file as a response, if needed
                return response($pdfData)
                    ->header('Content-Type', 'application/pdf')
                    ->header('Content-Disposition', 'attachment; filename="your-pdf-file.pdf"');
            } else {
                // Handle the case when the request to Express was not successful
                return response()->json(["data" => $res, "message" => "DOWNLOAD_PDF_FAILED"], $res->status());
            }
        } catch (\Exception $e) {
            return response()->json(["data" => $e, "message" => "DOWNLOAD_PDF_FAILED"], 500);
        }
    }
}