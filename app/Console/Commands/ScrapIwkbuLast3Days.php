<?php

namespace App\Console\Commands;

use App\Models\DasiUser;
use App\Models\Official;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class ScrapIwkbuLast3Days extends Command
{
    protected $signature = "scrap:last-3-days";

    public function handle()
    {
        $userData = DasiUser::where("username", "!=", "")->where("password", "!=", "")->first();
        $officialData = Official::orderBy("id")
//             ->where("id", ">=", 111)
            // ->orWhere("id", 107)
            ->get();
        $officialCodes = [];
        foreach ($officialData as $datum)
        {
            array_push($officialCodes, $datum["code"]);
        }
        $payload["username"] = $userData["username"];
        $payload["password"] = $userData["password"];
        $payload["reqType"] = "last-3-days";
        $payload["codes"] = $officialCodes;
        $url = env("SCRAPER_URL") . "/scrap/iwkbu/kantor-code";
        $res = Http::post($url, $payload);
        echo $res->body();
    }
}
