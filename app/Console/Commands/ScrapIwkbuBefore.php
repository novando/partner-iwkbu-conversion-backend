<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Official;
use App\Models\DasiUser;
use App\Helpers\DateHelper;

class ScrapIwkbuBefore extends Command
{
    protected $signature = "scrap:iwkbu-before";

    public function handle()
    {
        $oneWeekFromNow = DateHelper::modifyDateToIso("now", "+1 Day");
        $userData = DasiUser::where("username", "!=", "")->where("password", "!=", "")->first();
        $officialData = Official::orderBy("id")
//            ->whereIn("id", [55,63,71,79])
//             ->where("id", 51)
            // ->orWhere("id", 107)
            ->get();
        $officialCodes = [];
        foreach ($officialData as $datum)
        {
            array_push($officialCodes, $datum["code"]);
        }
        $payload["username"] = $userData["username"];
        $payload["password"] = $userData["password"];
        $payload["reqType"] = "iwkbu-before";
        $payload["month"] = DateHelper::dateToMonthNumberString($oneWeekFromNow);
        $payload["codes"] = $officialCodes;
        $url = env("SCRAPER_URL") . "/scrap/iwkbu/kantor-code";
        $res = Http::post($url, $payload);
        echo $res->body();
    }
}
