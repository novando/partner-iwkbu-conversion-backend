<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Official;
use App\Models\DasiUser;
use App\Helpers\DateHelper;

class ScrapIwkbuCurrent extends Command
{
    protected $signature = "scrap:iwkbu-current";

    public function handle()
    {

        $currentDate = DateHelper::modifyDateToIso("now", "-1 Month");
        $userData = DasiUser::where("username", "!=", "")->where("password", "!=", "")->first();
        $officialData = Official::orderBy("id")
             ->where("id", ">", 96)
            // ->orWhere("id", 107)
            ->get();
        $officialCodes = [];
        foreach ($officialData as $datum)
        {
            array_push($officialCodes, $datum["code"]);
        }
        $payload["username"] = $userData["username"];
        $payload["password"] = $userData["password"];
        $payload["reqType"] = "iwkbu-current";
        $payload["month"] = DateHelper::dateToMonthNumberString($currentDate);
        $payload["codes"] = $officialCodes;
        $url = env("SCRAPER_URL") . "/scrap/iwkbu/kantor-code";
        $res = Http::post($url, $payload);
        echo $res->body();
    }
}
