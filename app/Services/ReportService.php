<?php

namespace App\Services;

use App\Helpers\DateHelper;
use App\Models\Iwkbu;
use Illuminate\Support\Facades\DB;

class ReportService
{
  public static function officeSummary(string $since, string $till, string $hqCode = null, string $officialName = null)
  {
    [$lastSince, $lastTill] = DateHelper::rangeDateToLastYearRangeMariaArrayString($since, $till);
    [$currSince, $currTill] = DateHelper::rangeDateToRangeMariaArrayString($since, $till);
    $iwkbuIds = Iwkbu::select("iwkbus.id")
        ->join("officials", "officials.id", "=", "iwkbus.official_id")
        ->join("official_hqs", "official_hqs.id", "=", "officials.official_hq_id")
        ->join("official_names", "official_names.id", "=", "officials.official_name_id")
        ->where("official_hqs.code", "LIKE", "%$hqCode%")
        ->where("official_names.name", "LIKE", "%$officialName%")
        ->groupBy("iwkbus.id")
        ->pluck('iwkbus.id');
    if (count($iwkbuIds->toArray()) < 1) return response()->json(["data" => [], "message" => "DATA_NOT_FOUND"], 200);
    $officialDetailData = DB::select("
        SELECT oh.code, on2.name, SUM(price_before) AS price_before, SUM(price_after) AS price_after, SUM(qty_before) AS qty_before, SUM(qty_after) AS qty_after
        FROM (
            SELECT iwkbu_id, deleted_at, SUM(price_before) AS price_before, SUM(price_after) AS price_after, SUM(qty_before) AS qty_before, SUM(qty_after) AS qty_after
            FROM (
                SELECT id AS iwkbu_id, deleted_at, 0 AS price_before, 0 AS price_after, 0 AS qty_before, 0 AS qty_after
                FROM iwkbus iw
                UNION
                SELECT iwkbu_id, deleted_at, price AS price_before, 0 AS price_after, 1 AS qty_before, 0 AS qty_after
                FROM iwkbu_year_befores iyb
                WHERE recorded BETWEEN '".$lastSince."' AND '".$lastTill."'
                UNION
                SELECT iwkbu_id, deleted_at, 0 AS price_before, price AS price_after, 0 AS qty_before, 1 AS qty_after
                FROM iwkbu_year_befores iyc
                WHERE recorded BETWEEN '".$currSince."' AND '".$currTill."'
            ) AS u
            WHERE
                u.deleted_at IS NULL
                AND iwkbu_id IN (".implode(',', $iwkbuIds->toArray()).")
            GROUP BY iwkbu_id, u.deleted_at
        ) AS m
        JOIN iwkbus i ON i.id = m.iwkbu_id
        JOIN officials o ON o.id = i.official_id
        JOIN official_hqs oh ON oh.id = o.official_hq_id
        JOIN official_names on2 ON on2.id = o.official_name_id
        GROUP BY on2.name, oh.code
        ORDER BY oh.code ASC");
    $reportDto = [];
    foreach ($officialDetailData as $datum) {
        array_push($reportDto, [
            "hq" => $datum->code,
            "office" => $datum->name,
            "currQty" => (int)$datum->qty_after,
            "currIncome" => (double)$datum->price_after,
            "lastQty" => (int)$datum->qty_before,
            "lastIncome" => (double)$datum->price_before,
            "sumQty" => (int)$datum->qty_after - (int)$datum->qty_before,
            "sumIncome" => (double)$datum->price_after - (double)$datum->price_before,
        ]);
    }
    return $reportDto;
  }

  public static function conversionSummary (string $lastSince, string $lastTill, string $hqCode = null, string $officialName = null)
  {
    $iwkbuIds = Iwkbu::select("iwkbus.id")
        ->join("officials", "officials.id", "=", "iwkbus.official_id")
        ->join("official_hqs", "official_hqs.id", "=", "officials.official_hq_id")
        ->join("official_names", "official_names.id", "=", "officials.official_name_id")
        ->where("official_hqs.code", "LIKE", "%$hqCode%")
        ->where("official_names.name", "LIKE", "%$officialName%")
        ->groupBy("iwkbus.id")
        ->pluck('iwkbu.id');
    $detailData = DB::select("
        SELECT r.id, r.name, `desc`, score, COUNT(recorded) as qty, SUM(total) as total
        FROM (
            SELECT *, SUM(price) AS total, MAX(reason_id) AS reason
            FROM (
                SELECT id AS iwkbu_id, deleted_at, NULL AS iwkbu_date, NULL AS recorded, NULL AS swdkllj, 0 AS price, NULL AS reason_id
                FROM iwkbus iw
                UNION
                SELECT iwkbu_id, deleted_at, iwkbu_date, recorded, swdkllj, (price * -1) AS price, NULL AS reason_id
                FROM iwkbu_year_befores iyb
                UNION
                SELECT iwkbu_id, deleted_at, iwkbu_date, DATE_SUB(recorded, INTERVAL 1 YEAR), swdkllj, price, reason_id
                FROM iwkbu_year_befores iyc
            ) AS u
            WHERE
                recorded BETWEEN
                    '".$lastSince."' AND '".$lastTill."'
                AND u.deleted_at IS NULL
                AND iwkbu_id IN (".implode(',', $iwkbuIds->toArray()).")
            GROUP BY iwkbu_id
        ) AS m
        JOIN reasons r ON r.id = m.reason
        JOIN scores s ON s.id = r.score_id
        GROUP BY r.name, r.desc, s.score, r.id
        ORDER BY r.score_id ASC, r.id ASC");
    $detailDto = [];
    foreach ($detailData as $datum) {
        array_push($detailDto, [
            "name" => $datum->name,
            "price" => (double)$datum->total,
            "score" => (int)$datum->score,
            "qty" => (int)$datum->qty,
        ]);
    }
    return [$iwkbuIds, $detailDto];
  }
}
